#!/bin/bash

script_start_dir=$PWD

echo "Initialize usefull script variables"
projects_root=~/Projects
minetest_git=https://github.com/minetest/minetest
minetest_game_git=https://github.com/minetest/minetest_game

echo "Create ${projects_root} subfolder under home directory if not yet done ..."
[[ -d ${projects_root} ]] || mkdir -p ${projects_root}
echo "... and go within"
cd ${projects_root}

echo "Fetching all Minetest's versions in git repository"
i=0
tag_index=
versions=( $( git ls-remote --heads $minetest_git | sed "s:^.*refs/heads/::g" ) )
for version in "${versions[@]}"; do
	i=$(( i + 1 ))
	echo $i\) $version
done
while [[ -z "$tag_index" || "$tag_index" -lt "1" || "$tag_index" -gt "${#versions[@]}" ]]; do
    read -p "Please chose one version [1-${#versions[@]}] --> " tag_index
done

branch=${versions[$tag_index - 1]}
version=$( echo $branch | sed 's/stable-//g' )
name=minetest-$version
game_name=minetest_game-$version
server_name=minetestserver-$version

if [[ -d $script_start_dir/$name && -f $script_start_dir/$name/package.yml ]]; then
	if [[ -d $name/.git ]]; then
	    echo "Update the Minetest local git repository"
	    cd $name
	    git stash && git pull
	    cd ..
	else
	    echo "Clone the $name git repository"
	    git clone --single-branch --branch $branch $minetest_git $name
	fi

	if [[ -d $game_name/.git ]]; then
	    echo "Update the Minetest local git repository"
	    cd $game_name
	    git stash && git pull
	    cd ..
	else
	    echo "Clone the $game_name git repository"
	    git clone --single-branch --branch $branch $minetest_game_git $game_name
	fi

	echo "Creating a build directory"
	[[ -d build ]] && rm -r build
	mkdir build

	echo "Making the $name git repository a tarball"
	tar czf build/$name.tar.gz $name
	minetest_tarball_sha256sum=$( sha256sum build/$name.tar.gz | cut -d\  -f1 )

	echo "Making the $game_name git repository a tarball"
	tar czf build/$game_name.tar.gz $game_name
	minetest_game_tarball_sha256sum=$( sha256sum build/$game_name.tar.gz | cut -d\  -f1 )

	cp $script_start_dir/$name/* build/

	cd build

	sed --in-place "
	s|/minetest/|/$name/|g;
	s|#minetest_name#|$name|g;
	s|#minetest_version#|$version|g;
	s|#minetest_tarballdir#|file:///|g;
	s|#minetest_tarball#|$(realpath $name.tar.gz)|g;
	s|#minetest_tarball_sha256sum#|$minetest_tarball_sha256sum|g;
	s|#minetest_game_tarball#|$(realpath $game_name.tar.gz)|g;
	s|#minetest_game_tarball_sha256sum#|$minetest_game_tarball_sha256sum|g;
	s|#minetest_game_name#|$game_name|g;
	s|#minetest_server_name#|$server_name|g;
	" package.yml

	echo "Make sure Solus is updated"
	#sudo eopkg upgrade

	echo "Install and init solbuild package if not yet done"
	#! ( eopkg list-installed | grep '^solbuild ' ) ||  ( sudo eopkg install solbuild && sudo solbuild init -u )

	echo "Build the current minetest version"
	sudo solbuild build

	echo "Install the minetest package"
	#sudo eopkg install ${name}-1-x86_64.eopkg
else
	echo "$name is not yet supported by this script"
	echo "This script has terminated"
	cd $script_start_dir
	exit 1
fi

cd $script_start_dir
exit
